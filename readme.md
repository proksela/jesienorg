Jesien Linuksowa
================

Ogólnie
-------

* Wykorzystałem narzędzie [Lektor](https://www.getlektor.com/)
* Bazowy motyw to Display ze strony [freehtml5.co](https://freehtml5.co/) na bazie Bootstrap 3.
* Własne style napisałem w SASS
* Są już przygotowane dwie wersje językowe PL i EN.
* Żeby wiedzieć co gdzie można znaleźć należy spojrzeć do dokumentacji Lektora. Wszystko jest zgodnie ze standardem.

Struktura
---------

* W katalogu assets wykorzystane obrazki, style i JS.
* W `content/` większośc katalogów zawiera standardowe strony, 
`news/` to wiadomości wyświetlane na stronie głównej, 
`agenda/` to wydarzenia w agendzie z podziałem na dni,
`faq/` zawiera pary pytanie-odpowiedź. 
* W `databags/` znajdują się teksty, które pojawiają się w różnych miejscach na stronie w dwóch wersjach językowych.
    * `general.ini` to różne krótkie teksty porozrzuce na stronie, czyli boczny panel sponsorów, stopka, strona główna, menu.
    * `main-nav.ini` to elementy, które mają pojawić się w głównej nawigacji. Są to pary adres - napis.
    * `main-details-nav.ini` to elementy, które pojawią się w rozwijanym menu pod `Details`/`Szczegóły`
    
Wykorzystanie
-------------

Jak coś zmieniać dodawać? Jak trzeba zmienić wygląd to należy spojrzeć do `templates/` i `assets/`, 
ale dodawanie wszystkie da się zrealizować w adminie lektora. Są tam wszystkie elementy strony i przykładowa zawartość. 
Zanim zaczniecie coś zmieniać zobaczcie jak to działa. Są tam też przyciski Polski/English i one wystarczają do zmiany językowej treści,
Żeby zmienić ścieżki stron trzeba spojrzeć do „System Fields” - URL slug.

Problemy
--------

* Nawigacja psuje się przy szerokim ekranie
* Menu główne wydaje się trochę puste